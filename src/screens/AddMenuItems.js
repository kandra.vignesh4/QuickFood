import React, { Component } from "react";
// import Navbar from '../components/Navbar';
import Navbar2 from "../components/Navbar2";
import Footer from "../components/Footer";
// import { addItem } from '../config/firebase';
import { addItem } from "../ServiceLayer/Restaurant";
import Swal from "sweetalert2";

import "bootstrap/dist/css/bootstrap.css";
import "../App.css";

export default class AddMenuItems extends Component {
  constructor() {
    super();
    this.state = {
      itemImageLable: "Choose image",
      itemTitle: "",
      itemIngredients: "",
      itemPrice: "",
      itemImage: null,
      //chooseItemType: "",
      showError: false,
      showErrorItemTitle: false,
      registerFormError: "",
      errorItemTitle: "",
      errorItemIngredients: "",
      errorPrice: "",
      errorItemImages: "",
      errorChooseItemType: "",
      chooseItemType: "kebabs",
    };

    this.handleItemPrice = this.handleItemPrice.bind(this);
    this.handleItemTitle = this.handleItemTitle.bind(this);
    this.handleItemIngredients = this.handleItemIngredients.bind(this);
    this.handleItemPhoto = this.handleItemPhoto.bind(this);
    this.handleChooseType = this.handleChooseType.bind(this);
    this.handleAddYourItemBtn = this.handleAddYourItemBtn.bind(this);
    //this.handleUserItem = this.handleUserItem.bind(this);
  }

  handleItemPrice(e) {
    const ItemPrice = e;
    if (ItemPrice != 0) {
      this.setState({
        showErrorItemPrice: false,
        errorPrice: "",
        itemPrice: ItemPrice,
      });
    } else {
      this.setState({
        showErrorItemPrice: true,
        errorPrice: "Invalid item price.",
        itemPrice: "",
      });
    }
  }

  handleItemIngredients(e) {
    const itemIngredients = e;
    if (itemIngredients.length == 0) {
      this.setState({
        showErrorItemIngredients: true,
        errorItemIngredients: "Invalid item ingredients.",
        itemIngredients: "",
      });
    } else {
      this.setState({
        showErrorItemIngredients: false,
        errorItemIngredients: "",
        itemIngredients: itemIngredients,
      });
    }
  }

  handleItemTitle(e) {
    const itemTitle = e;
    const itemTitleFormate = /^([A-Za-z.\s_-]).{2,}$/;
    console.log("item title", itemTitle);
    if (itemTitle.match(itemTitleFormate)) {
      this.setState({
        showErrorItemTitle: false,
        errorItemTitle: "",
        itemTitle: itemTitle,
      });
    } else {
      this.setState({
        showErrorItemTitle: true,
        errorItemTitle: "Invalid item title.",
        itemTitle: "",
      });
    }
  }
  handleItemPhoto(e) {
    const itemImage = e;
    if (itemImage == null) {
      this.setState({
        showErrorPhotoImage: true,
        errorItemImages: "please select a Item image",
        itemImageLable: "Choose image",
        itemImage: "",
      });
    } else {
      this.setState({
        showErrorPhotoImage: false,
        errorItemImages: "",
        itemImageLable: e.target.files[0].name,
        itemImage: e.target.files[0],
      });
    }
  }
  // handleChooseType(e) {
  //   const chooseItemType = e;
  //   if (chooseItemType.length == 0) {
  //     this.setState({
  //       showErrorChooseItemtype: true,
  //       errorChooseItemType: "Must be selected any one.",
  //       chooseItemType: "",
  //     });
  //   } else {
  //     this.setState({
  //       showErrorChooseItemtype: false,
  //       errorChooseItemType: "",
  //       chooseItemType: chooseItemType,
  //     });
  //   }
  // }
  handleChooseType(e) {
    this.setState({
      chooseItemType: e.target.value,
    });
    console.log("item is ", e.target.value);
  }

  async handleAddYourItemBtn() {
     this.refs.btn.setAttribute("disabled", "disabled");
    const {
      itemTitle,
      itemIngredients,
      itemPrice,
      itemImage,
      chooseItemType,
    } = this.state;

    if (itemTitle.length == 0) {
      this.setState({
        showErrorItemTitle: true,
        errorItemTitle: "Invalid item title.",
        itemTitle: "",
      });
      if (itemIngredients.length == 0) {
        this.setState({
          showErrorItemIngredients: true,
          errorItemIngredients: "Invalid item ingredients.",
          itemIngredients: "",
        });
        
        if (itemPrice == 0) {
          this.setState({
            showErrorItemPrice: true,
            errorPrice: "Invalid item price.",
          });
          if (itemImage == null) {
            this.setState({
              showErrorPhotoImage: true,
              errorItemImages: "please select a Item image",
              itemImageLable: "Choose image",
              itemImage: "",
            });
            if (chooseItemType.length == 0) {
              this.setState({
                showErrorChooseItemtype: true,
                errorChooseItemType: "Must be selected any one.",
              });
            }
          }
        }
      }
    }
    if (itemIngredients.length == 0) {
      this.setState({
        showErrorItemIngredients: true,
        errorItemIngredients: "Invalid item ingredients.",
        itemIngredients: "",
      });
      if (itemPrice == 0) {
        this.setState({
          showErrorItemPrice: true,
          errorPrice: "Invalid item price.",
        });
        if (itemImage == null) {
          this.setState({
            showErrorPhotoImage: true,
            errorItemImages: "please select a Item image",
            itemImageLable: "Choose image",
            itemImage: "",
          });
          if (chooseItemType.length == 0) {
            this.setState({
              showErrorChooseItemtype: true,
              errorChooseItemType: "Must be selected any one.",
            });
          }
        }
      }
    }

    if (itemPrice == 0) {
      this.setState({
        showErrorItemPrice: true,
        errorPrice: "Invalid item price.",
      });
      if (itemImage == null) {
        this.setState({
          showErrorPhotoImage: true,
          errorItemImages: "please select a Item image",
          itemImageLable: "Choose image",
          itemImage: "",
        });
        if (chooseItemType.length == 0) {
          this.setState({
            showErrorChooseItemtype: true,
            errorChooseItemType: "Must be selected any one.",
          });
        }
      }
    }
    if (itemImage == null) {
      this.setState({
        showErrorPhotoImage: true,
        errorItemImages: "please select a Item image",
        itemImageLable: "Choose image",
        itemImage: "",
      });
      if (chooseItemType.length == 0) {
        this.setState({
          showErrorChooseItemtype: true,
          errorChooseItemType: "Must be selected any one.",
        });
      }
    }

    if (chooseItemType.length == 0) {
      this.setState({
        showErrorChooseItemtype: true,
        errorChooseItemType: "Must be selected any one.",
      });
    } else {
      const itemDetails = {
        itemTitle: itemTitle,
        itemIngredients: itemIngredients,
        itemPrice: itemPrice,
        itemImage: itemImage,
        chooseItemType: chooseItemType,
        propsHistory: this.props.history,
      };
      try {
        const addItemReturn = await addItem(itemDetails);
        console.log(addItemReturn);
        Swal.fire({
          title: "Success",
          text: addItemReturn,
          type: "success",
        }).then(() => {
          this.props.history.push("/my-foods");
        });
      } catch (error) {
        console.log("Error in add menu items => ", error);
        // Swal.fire({
        //     title: 'Unable to add food',
        //     text: "try again in some time",
        //     type: 'error',
        // })
      }
    }
  }

  render() {
    const {
      itemImageLable,
      showError,
      showErrorItemTitle,
      showErrorItemIngredients,
      showErrorChooseItemtype,
      showErrorItemPrice,
      showErrorPhotoImage,
      registerFormError,
      errorItemTitle,
      errorItemIngredients,
      errorChooseItemType,
      errorItemImages,
      errorPrice,
      userItem,
    } = this.state;
    return (
      <div>
        <div className="container-fluid register-cont1">
          <div className="">
            {/* <Navbar history={this.props.history} /> */}
            <Navbar2 history={this.props.history} />
            <div className="container register-cont1-text">
              <h1 className="text-uppercase text-white text-center mb-4">
                <strong>Add Your Best Food Items</strong>
              </h1>
            </div>
          </div>
        </div>
        <div className="container-fluid py-5 bg-light">
          <div className="col-lg-6 col-md-6 col-sm-12 mx-auto bg-white shadow p-4">
            <h2 className="text-center mb-4">Add Menu Items</h2>
            <form action="javascript:void(0)">
              <div className="form-row">
                <div className="form-group col-md-6">
                  <label htmlFor="itemTitle">
                    <b>Item Title</b>
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="itemTitle"
                    placeholder="Full name of dish"
                    onKeyUp={(e) => this.handleItemTitle(e.target.value)}
                  />
                  {showErrorItemTitle ? (
                    <p className="text-danger mb-0">{errorItemTitle}</p>
                  ) : null}
                </div>
                <div className="form-group col-md-6">
                  <label htmlFor="itemIngredients">
                    <b>Item Ingredients</b>
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="itemIngredients"
                    placeholder="Item Ingredients Name"
                    onKeyUp={(e) => this.handleItemIngredients(e.target.value)}
                  />
                  {showErrorItemIngredients ? (
                    <p className="text-danger mb-0">{errorItemIngredients}</p>
                  ) : null}
                </div>
              </div>
              <div className="form-row">
                <div className="form-group col-md-6">
                  <label htmlFor="itemPrice">
                    <b>Price</b>
                  </label>
                  <input
                    type="number"
                    className="form-control"
                    id="itemPrice"
                    placeholder="Price in number"
                    onKeyUp={(e) => this.handleItemPrice(e.target.value)}
                  />
                  {showErrorItemPrice ? (
                    <p className="text-danger mb-0">{errorPrice}</p>
                  ) : null}
                </div>
                <div className="form-group col-md-6">
                  <label className="mb-2">
                    <b>Item Image</b>
                  </label>
                  <div className="custom-file">
                    <input
                      type="file"
                      className="custom-file-input"
                      id="itemImage"
                      accept="image/x-png,image/gif,image/jpeg"
                      onChange={this.handleItemPhoto}
                    />
                    <label className="custom-file-label" htmlFor="itemImage">
                      {itemImageLable}
                    </label>
                    {showErrorPhotoImage ? (
                      <p className="text-danger mb-0">{errorItemImages}</p>
                    ) : null}
                  </div>
                </div>
              </div>
              <label className="mb-2">
                <b>Choose Item Type</b>
              </label>

              <select
                id="userItem"
                className="form-control"
                 value={userItem}
                placeholder="choose from dropdown"
                onClick={this.handleChooseType}
              >
                <option>Kebabs</option>
                <option>Chicken</option>
                <option>Burgers</option>
                <option>Biryani</option>
                <option>Sauces</option>
                <option>Vegetarian</option>
                <option>Bread</option>
                <option>Specials</option>

                {/* <input
                      className="custom-control-input"
                      id="chicken"
                      value="chicken"
                      name="chooseItemType"
                      onChange={(e) => this.handleChooseType(e.target.value)}
                    />
                    <label className="custom-control-label" htmlFor="chicken">
                      Chicken
                    </label>
                
                    <input
                     
                      className="custom-control-input"
                      id="burgers"
                      value="burgers"
                      name="chooseItemType"
                      onChange={(e) => this.handleChooseType(e.target.value)}
                    />
                    <label className="custom-control-label" htmlFor="burgers">
                      Burgers
                    </label>
                  
                
                    <input
                      
                      className="custom-control-input"
                      id="biryani"
                      value="biryani"
                      name="chooseItemType"
                      onChange={(e) => this.handleChooseType(e.target.value)}
                    />
                    <label className="custom-control-label" htmlFor="biryani">
                      Biryani
                    </label>
                 
              
                    <input
                     
                      className="custom-control-input"
                      id="sauces"
                      value="sauces"
                      name="chooseItemType"
                      onChange={(e) => this.handleChooseType(e.target.value)}
                    />
                    <label className="custom-control-label" htmlFor="sauces">
                      Sauces
                    </label>
               
                    <input
                    
                      className="custom-control-input"
                      id="vegetarian"
                      value="vegetarian"
                      name="chooseItemType"
                      onChange={(e) => this.handleChooseType(e.target.value)}
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="vegetarian"
                    >
                      Vegetarian
                    </label>
                  
               
                    <input
                      
                      className="custom-control-input"
                      id="bread"
                      value="bread"
                      name="chooseItemType"
                      onChange={(e) => this.handleChooseType(e.target.value)}
                    />
                    <label className="custom-control-label" htmlFor="bread">
                      Bread
                    </label>
                  
                    <input
                     
                      className="custom-control-input"
                      id="specials"
                      value="specials"
                      name="chooseItemType"
                      onChange={(e) => this.handleChooseType(e.target.value)}
                    />
                    <label className="custom-control-label" htmlFor="specials">
                      Specials
                    </label> */}
              </select>

              {showErrorChooseItemtype ? (
                <p className="text-danger mb-0">{errorChooseItemType}</p>
              ) : null}

              <button
                type="submit"
                ref="btn"
                className="btn btn-warning text-uppercase mb-3"
                style={{ marginTop: "20px" }}
                onClick={this.handleAddYourItemBtn}
              >
                <b>Add your item</b>
              </button>
            </form>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
