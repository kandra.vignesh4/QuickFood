import React, { Component, useState } from "react";

import Navbar2 from "../components/Navbar2";
import Footer from "../components/Footer";
import axios from "axios";
import { connect } from "react-redux";
import { orderNow } from "../ServiceLayer/Order";
import Swal from "sweetalert2";
import Modal from "react-bootstrap/Modal";
import { restaurant_list } from "../ServiceLayer/Restaurant";
import "bootstrap/dist/css/bootstrap.css";
import "../App.css";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import RestaurantDetails from "./RestaurantDetails";
class RestaurantProfile extends Component {
  constructor() {
    super();
    this.state = {};

    


  }

  // async componentMount() {
  //   let restaurant = {};
  //   var token = localStorage.getItem("securityToken");
  //   const options = {
  //     url:
  //     process.env.REACT_APP_NodeJS_ServiceURL + "/api/v1/auth/me",
  //     method: "GET",
  //     headers: {
  //       Accept: "application/json",
  //       "Content-Type": "application/json;charset=UTF-8",
  //       Authorization: `Bearer ${token}`,
  //     },
  //   };
  //   await axios(options)
  //     .then((response) => {
  //       console.log(response);
  //       restaurant = response.data.data;
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //     });
  //   this.setState({
  //     userName: restaurant.UserName,
      
  //   });
  // }
  static getDerivedStateFromProps(props) {
    const { user } = props
    console.log('uvbjhbkj',user.userName);
    console.log('uvbjhbkj',user.userCity);
    console.log('uvbjhbkj',user.userProfileImageUrl);
    
   
    return {
        userName: user.userName,
        phoneNumber: user.phoneNumber,
        userEmail: user.userEmail,
        userCity: user.userCity,
        userPincode: user.userPincode,
        userAddress: user.userAddress,
        userCountry: user.userCountry,
        userGender: user.userGender,
        userAge: user.userAge,
        userProfileImage: user.userProfileImageUrl,
        chooseservice: user.chooseservice,
        choosetype: user.choosetype

        
    }

    
    
}
 //Handleclose is for closing the dialog box
 handleClose = () => this.props.handleClose(false);



  handleUpdateBtn() {
    console.log();
  }


  handleDeleteBtn() {}



  render() {
    const { userName, userEmail, userCity, userPincode, userAddress, userCountry, userGender, userAge, userProfileImage, phoneNumber, chooseservice, choosetype} = this.state;
    console.log('getting the values',phoneNumber);
    return (
      <div>
        <div className="container-fluid res-details-cont1">
          <div className="">
            {/* <Navbar history={this.props.history} /> */}
            <Navbar2 history={this.props.history} />

            <div className="container px-0 res-details-cont1-text mx-0">
              <div className="container">
                <div className="row"></div>
              </div>
            </div>
          </div>
        </div>
        <div className="card mb-3">
          <div className="row no-gutters">
            <div className="col-md-4">
              <img src={userProfileImage} className="card-img" style={{ width: "15em", height: "15em" }}
                      alt="Natural Healthy Food"/>
            </div>
            <div className="col-md-8">
              <div className="card-body">
                <h5 className="card-title">Restaurant Name{userName}</h5>
                <div className="row">
                  <p className="card-text">
                    Base prepared fresh daily. Extra toppings are available in
                    choose extra Choose you sauce: Go for BBQ sauce or piri piri
                    sauce on your pizza base for no extra cost. Choose your cut:
                    Triangular, square, fingers or Un cut on any size pizza
                  </p>
                </div>
                <div className="row">
                  <b>
                    Phone Number:{phoneNumber}
                    {console.log(this.props.location)}
                  </b>
                </div>
                <div className="row">
                  <b>Email:</b>{userEmail}
                </div>
                <div className="row">
                  <b>Address:</b>{userAddress}
                </div>
                <div className="row">
                  <div className="col-lg-12 col-md-12 col-sm-12">
                    <button
                      type="submit"
                      className="btn btn-warning text-uppercase mb-3"
                      onClick={this.handleUpdateBtn}
                    >
                      <b>Update</b>
                    </button>{" "}
                    <button
                      type="submit"
                      className="btn btn-warning text-uppercase mb-3"
                      onClick={this.handleDeleteBtn}
                    >
                      <b>Delete</b>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>


      // restaurant copy 

      // <div>
      //   <div className="container-fluid register-cont1">
      //     <div className="">
      //       {/* <Navbar history={this.props.history} /> */}
      //       <Navbar2 history={this.props.history} />
      //       <div className="container register-cont1-text">
      //         <h1 className="text-uppercase text-white text-center mb-4">
      //           <strong>Register User And Add Restaurant</strong>
      //         </h1>
      //       </div>
      //     </div>
      //   </div>
      //   <div className="container-fluid py-5 bg-light">
      //     <div className="col-lg-6 col-md-6 col-sm-12 mx-auto bg-white shadow p-4">
      //       <h2 className="text-center mb-4">Update Restaurant</h2>
      //       <form action="javascript:void(0)">
      //         <div className="form-row">
      //           <div className="form-group col-md-6">
      //             <label htmlFor="userFullName">Restaurant Name</label>
      //             <input
      //               type="text"
      //               className="form-control"
      //               id="userName"
      //               placeholder="Restaurant Name"
      //               onKeyUp={(e) => this.handleUserName(e.target.value)}
      //             />
      //             {showErrorUsername ? <small className="text-danger mb-0">{registerFormErrorUsername}</small> : null}
      //           </div>
      //           <div className="form-group col-md-6">
      //             <label htmlFor="userEmail">Email</label>
      //             <input
      //               type="email"
      //               autoComplete="false"
      //               className="form-control"
      //               id="userEmail"
      //               placeholder="Email"
      //               onKeyUp={(e) => this.handleUserEmail(e.target.value)}
      //             />
      //             {showErrorEmail ? <small className="text-danger mb-0">{registerFormErrorEmail}</small> : null}
      //             {showError ? <small className="text-danger mb-0">{registerFormError}</small> : null}
      //           </div>
      //         </div>
      //         <div className="form-row">
      //           <div className="form-group col-md-6">
      //             <label htmlFor="userPassword">Password</label>
      //             <input
      //               type="password"
      //               autoComplete="new-password"
      //               className="form-control"
      //               id="userPassword"
      //               placeholder="Password"
      //               onKeyUp={(e) => this.handleUserPassword(e.target.value)}
      //             />
      //             {showErrorPassword ? <small className="text-danger mb-0">{registerFormErrorPassword}</small> : null}
      //           </div>
      //           <div className="form-group col-md-6">
      //             <label htmlFor="userConfirmPassword">Confirm Password</label>
      //             <input
      //               type="password"
      //               className="form-control"
      //               id="userConfirmPassword"
      //               placeholder="Password"
      //               onKeyUp={(e) =>
      //                 this.handleUserConfirmPassword(e.target.value)
      //               }
      //             />
      //             {showErrorCPassword ? <small className="text-danger mb-0">{registerFormErrorCPassword}</small> : null}
      //           </div>
      //         </div>
      //         <div className="form-row">
      //           <div className="form-group col-md-6">
      //             <label htmlFor="userAddress">Address</label>
      //             <input
      //               type="text"
      //               max="40"
      //               className="form-control"
      //               id="userAddress"
      //               placeholder="Address"
      //               onKeyUp={(e) => this.handleUserAddress(e.target.value)}
      //             />
      //             {showErrorAddress ? <small className="text-danger mb-0">{registerFormErrorAddress}</small> : null}
      //           </div>
      //           <div className="form-group col-md-6">
      //             <label htmlFor="userCity">City</label>
      //             <input
      //               type="text"
      //               className="form-control"
      //               id="userCity"
      //               placeholder="City"
      //               onKeyUp={(e) => this.handleUserCity(e.target.value)}
      //             />
      //             {showErrorCity ? <small className="text-danger mb-0">{registerFormErrorCity}</small> : null}
      //           </div>
      //         </div>
      //         <div className="form-row">
      //           <div className="form-group col-md-6">
      //             <label htmlFor="userCountry">Country</label>
      //             <input
      //               type="text"
      //               className="form-control"
      //               id="userCountry"
      //               placeholder="Country"
      //               onKeyUp={(e) => this.handleUserCountry(e.target.value)}
      //             />
      //             {showErrorCountry ? <small className="text-danger mb-0">{registerFormErrorCountry}</small> : null}
      //           </div>
      //           <div className="form-group col-md-6">
      //             <label htmlFor="userPincode">Pincode</label>
      //             <input
      //               type="number"
      //               className="form-control"
      //               id="userPincode"
      //               placeholder="Pincode"
      //               onKeyUp={(e) => this.handleUserPincode(e.target.value)}
      //             />
      //             {showErrorPincode ? <small className="text-danger mb-0">{registerFormErrorPincode}</small> : null}
      //           </div>
      //         </div>
      //         <div className="form-row">
      //           <div className="form-group col-md-4">
      //             <label htmlFor="userGender">Gender</label>
      //             <select
      //               id="userGender"
      //               className="form-control"
      //               value={userGender}
      //               onChange={this.handleUserGender}
      //             >
      //               <option defaultValue>Male</option>
      //               <option>Female</option>
      //             </select>
      //           </div>
      //           <div className="form-group col-md-2">
      //             <label htmlFor="userAge">Age</label>
      //             <input
      //               type="number"
      //               className="form-control"
      //               id="userAge"
      //               onKeyUp={(e) => this.handleUserAge(e.target.value)}
      //             />
      //             {showErrorAge ? <small className="text-danger mb-0">{registerFormErrorAge}</small> : null}
      //           </div>
      //           <div className="form-group col-md-6">
      //             <p className="mb-2">Profile Image</p>
      //             <div className="custom-file">
      //               <input
      //                 type="file"
      //                 className="custom-file-input"
      //                 id="userProfileImage"
      //                 accept="image/x-png,image/gif,image/jpeg"
      //                 onChange={this.handleUserProfileImage}
      //               />
      //               <label
      //                 className="custom-file-label"
      //                 htmlFor="userProfileImage"
      //               >
      //                 {userProfileImageLable}
      //               </label>
      //               {showErrorProfileImage ? <small className="text-danger mb-0">{registerFormErrorProfileImage}</small> : null}
      //             </div>
      //           </div>
      //         </div>
      //         {/* phone number */}
      //         <div className="form-group">
      //           <label htmlFor="phonenumber">Phone Number</label>
      //           <input
      //             type="text"
      //             className="form-control"
      //             id="phonenumber"
      //             placeholder="Phone Number"
      //             onKeyUp={(e) => this.handlePhoneNumber(e.target.value)}
      //           />
      //           {showErrorPhoneNumber ? <small className="text-danger mb-0">{registerFormErrorPhoneNumber}</small> : null}
      //         </div>
      //         {/* phone number */}
      //         {/* added checkbox */}
      //         <label className="mb-2">Service</label>
              
      //          <div className="form-row">
      //           <div className="form-group col-md-2">
      //             <div className="custom-control custom-radio">
      //               <input
      //                 type="checkbox"
      //                 className="custom-control-input"
      //                 id="Breakfast"
      //                 value="BreakFast"
      //                 name="Breakfast"
      //                 onChange={(e) =>
      //                   this.setState({ chooseservice: e.target.value })
      //                 }
      //                 onKeyUp={(e) => this.handleservicetype(e.target.value)}
      //               />
      //               <label className="custom-control-label" htmlFor="Breakfast">
      //                 Breakfast
      //               </label>
      //             </div>
      //           </div>
      //           <div className="form-group col-md-2">
      //             <div className="custom-control custom-radio">
      //               <input
      //                 type="checkbox"
      //                 className="custom-control-input"
      //                 id="Lunch"
      //                 value="Lunch"
      //                 name="Lunch"
      //                 onChange={(e) =>
      //                   this.setState({ chooseservice: e.target.value })
      //                 }
      //                 onKeyUp={(e) => this.handleservicetype(e.target.value)}
      //               />
      //               <label className="custom-control-label" htmlFor="Lunch">
      //                 Lunch
      //               </label>
      //             </div>
      //           </div>
      //           <div className="form-group col-md-2">
      //             <div className="custom-control custom-radio">
      //               <input
      //                 type="checkbox"
      //                 className="custom-control-input"
      //                 id="Dinner"
      //                 value="Dinner"
      //                 name="Dinner"
      //                 onChange={(e) =>
      //                   this.setState({ chooseservice: e.target.value })
      //                 }
      //                 onKeyUp={(e) => this.handleservicetype(e.target.value)}
      //               />
      //               <label className="custom-control-label" htmlFor="Dinner">
      //                 Dinner
      //               </label>
      //             </div>
      //           </div>
      //           <div className="form-group col-md-2">
      //             <div className="custom-control custom-radio">
      //               <input
      //                 type="checkbox"
      //                 className="custom-control-input"
      //                 id="Snacks"
      //                 value="Snacks"
      //                 name="Snacks"
      //                 onChange={(e) =>
      //                   this.setState({ chooseservice: e.target.value })
      //                 }
      //                 onKeyUp={(e) => this.handleservicetype(e.target.value)}
      //               />
      //               <label className="custom-control-label" htmlFor="Snacks">
      //                 Snacks
      //               </label>
      //             </div>
      //           </div>
      //           <div className="form-group col-md-2">
      //             <div className="custom-control custom-radio">
      //               <input
      //                 type="checkbox"
      //                 className="custom-control-input"
      //                 id="Drinks"
      //                 value="Drinks"
      //                 name="Drinks"
      //                 onChange={(e) =>
      //                   this.setState({ chooseservice: e.target.value })
      //                 }
      //                 onKeyUp={(e) => this.handleservicetype(e.target.value)}
      //               />
      //               <label className="custom-control-label" htmlFor="Drinks">
      //                 Drinks
      //               </label>
      //             </div>
      //           </div>
      //           <div className="form-group col-md-2">
      //             <div className="custom-control custom-radio">
      //               <input
      //                 type="checkbox"
      //                 className="custom-control-input"
      //                 id="Dessert"
      //                 value="Dessert"
      //                 name="Dessert"
                      
      //                 onKeyUp={(e) => this.handleservicetype(e.target.value)}
      //               />
      //               <label className="custom-control-label" htmlFor="Dessert">
      //                 Dessert
      //               </label>
      //             </div>
      //           </div>
      //         </div> 
      //         {showErrorChooseServices ? <small className="text-danger mb-0">{errorChooseServices}</small> : null}
      //         {/* choose type */}
      //         <div className="form-group">
      //           <label className="mb-2">Choose Type</label>
      //           <div className="form-column">
      //             <div className="form-group col-md-4">
      //               <div className="custom-control custom-radio">
      //                 <input
      //                   type="checkbox"
      //                   className="custom-control-input"
      //                   id="Veg"
      //                   value="Veg"
      //                   name="Veg"
      //                   onChange={(e) =>
      //                     this.setState({ choosetype: e.target.value })
      //                   }
      //                 />
      //                 <label className="custom-control-label" htmlFor="Veg">
      //                   Veg
      //                 </label>
      //               </div>
      //             </div>
      //             <div className="form-group col-md-4">
      //               <div className="custom-control custom-radio">
      //                 <input
      //                   type="checkbox"
      //                   className="custom-control-input"
      //                   id="Non-Veg"
      //                   value="Non-Veg"
      //                   name="Non-Veg"
      //                   onChange={(e) =>
      //                     this.setState({ choosetype: e.target.value })
      //                   }
      //                 />
      //                 <label className="custom-control-label" htmlFor="Non-Veg">
      //                   Non-Veg
      //                 </label>
      //               </div>
      //             </div>
      //             <div className="form-group col-md-4">
      //               <div className="custom-control custom-radio">
      //                 <input
      //                   type="checkbox"
      //                   className="custom-control-input"
      //                   id="VegandNon-Veg"
      //                   value="VegandNon-Veg"
      //                   name="VegandNon-Veg"
      //                   onChange={(e) =>
      //                     this.setState({ choosetype: e.target.value })
      //                   }
      //                 />
      //                 <label
      //                   className="custom-control-label"
      //                   htmlFor="VegandNon-Veg"
      //                 >
      //                   Veg & Non-Veg
      //                 </label>
      //               </div>
      //             </div>
      //             <div className="form-group col-md-4">
      //               <div className="custom-control custom-radio">
      //                 <input
      //                   type="checkbox"
      //                   className="custom-control-input"
      //                   id="Juice"
      //                   value="Juice"
      //                   name="Juice"
      //                   onChange={(e) =>
      //                     this.setState({ choosetype: e.target.value })
      //                   }
      //                 />
      //                 <label className="custom-control-label" htmlFor="Juice">
      //                   Juice
      //                 </label>
      //               </div>
      //             </div>
      //             <div className="form-group col-md-4">
      //               <div className="custom-control custom-radio">
      //                 <input
      //                   type="checkbox"
      //                   className="custom-control-input"
      //                   id="Desserts"
      //                   value="Desserts"
      //                   name="Desserts"
      //                   onChange={(e) =>
      //                     this.setState({ choosetype: e.target.value })
      //                   }
      //                 />
      //                 <label className="custom-control-label" htmlFor="Desserts">
      //                   Dessert
      //                 </label>
      //               </div>
      //             </div>
      //             <div className="form-group col-md-4">
      //               <div className="custom-control custom-radio">
      //                 <input
      //                   type="checkbox"
      //                   className="custom-control-input"
      //                   id="Special"
      //                   value="Special"
      //                   name="Special"
      //                   onChange={(e) =>
      //                     this.setState({ choosetype: e.target.value })
      //                   }
      //                 />
      //                 <label className="custom-control-label" htmlFor="Special">
      //                   Special
      //                 </label>
      //               </div>
      //             </div>
      //             <div className="form-group col-md-4">
      //               <div className="custom-control custom-radio">
      //                 <input
      //                   type="checkbox"
      //                   className="custom-control-input"
      //                   id="Chinese"
      //                   value="Chinese"
      //                   name="Chinese"
      //                   onChange={(e) =>
      //                     this.setState({ choosetype: e.target.value })
      //                   }
      //                 />
      //                 <label className="custom-control-label" htmlFor="Chinese">
      //                   Chinese
      //                 </label>
      //               </div>
      //             </div>
      //             <div className="form-group col-md-4">
      //               <div className="custom-control custom-radio">
      //                 <input
      //                   type="checkbox"
      //                   className="custom-control-input"
      //                   id="Italian"
      //                   value="Italian"
      //                   name="Italian"
      //                   onChange={(e) =>
      //                     this.setState({ choosetype: e.target.value })
      //                   }
      //                 />
      //                 <label className="custom-control-label" htmlFor="Italian">
      //                   Italian
      //                 </label>
      //               </div>
      //             </div>
      //             <div className="form-group col-md-4">
      //               <div className="custom-control custom-radio">
      //                 <input
      //                   type="checkbox"
      //                   className="custom-control-input"
      //                   id="Contidental"
      //                   value="Contidental"
      //                   name="Drinks"
      //                   onChange={(e) =>
      //                     this.setState({ choosetype: e.target.value })
      //                   }
      //                 />
      //                 <label
      //                   className="custom-control-label"
      //                   htmlFor="Contidental"
      //                 >
      //                   Contidental
      //                 </label>
      //               </div>
      //             </div>

      //             <div className="form-group col-md-4">
      //               <div className="custom-control custom-radio">
      //                 <input
      //                   type="checkbox"
      //                   className="custom-control-input"
      //                   id="Others"
      //                   value="Others"
      //                   name="Others"
      //                   onChange={(e) =>
      //                     this.setState({ choosetype: e.target.value })
      //                   }
      //                 />
      //                 <label className="custom-control-label" htmlFor="Others">
      //                   Others
      //                 </label>
      //               </div>
      //             </div>
      //           </div>
      //         </div>
      //         {showErrorChoosetype ? <small className="text-danger mb-0">{errorChooseType}</small> : null}

      //         {/* end */}
      //         <label className="RestaurantOpen">Restaurant Open</label>

      //         {/*<div className="form-row">
      //           <div className="form-group col-md-6">
      //             <div className="form-group">
      //               <div className="custom-control custom-checkbox">
      //                 <input
      //                   type="checkbox"
      //                   className="custom-control-checkbox"
      //                 />
      //                 <label className="custom-control-input" htmlFor="Days">
      //                   Monday
      //                 </label>
      //                 <span className="OpenTime">
      //                   <input type="time" />
      //                 </span>
      //               </div>
      //             </div>
      //           </div>
      //         </div>*/}
      //         <div className="timings">
      //           <div className="form-row1">
      //             <div className="form-group col-md-111">
      //               <div className="custom-control custom-checkbox">
      //                 <input
      //                   type="checkbox"
      //                   id="checkbox1"
      //                   className="custom-control-input"
      //                   onChange={this.handledays}
      //                 />
      //                 <label
      //                   className="custom-control-label"
      //                   htmlFor="checkbox1"
      //                 >
      //                   Monday
      //                 </label>{" "}
      //               </div>
      //             </div>
      //             <div className="form-group col-md-111">
      //               <span>
      //                 <input type="time" id="opentime" 
                     
      //                 onChange={this.handletime}/>{" "}
      //               </span>
      //             </div>
      //             <div className="form-group col-md-111">
      //               <span>
      //                 <input type="time" id="closetime"
                      
                      
      //                 onKeyUp={(e) => this.handletime(e.target.value)} />
      //               </span>
      //             </div>
      //           </div>

      //           <div className="form-row1">
      //             <div className="form-group col-md-111">
      //               <div className="custom-control custom-checkbox">
      //                 <input
      //                   type="checkbox"
      //                   id="checkbox2"
      //                   className="custom-control-input"
      //                   onChange={this.handledays}
      //                 />
      //                 <label
      //                   className="custom-control-label"
      //                   htmlFor="checkbox2"
      //                 >
      //                   Tuesday
      //                 </label>{" "}
      //               </div>
      //             </div>
      //             <div className="form-group col-md-111">
      //               <span>
      //                 <input type="time" id="opentime"
      //                 onChange={this.handletime} />{" "}
                      
      //               </span>
      //             </div>
      //             <div className="form-group col-md-111">
      //               <span>
      //                 <input type="time" id="closetime"
      //                 onChange={this.handletime} />
                      
      //               </span>
      //             </div>
      //           </div>

      //           <div className="form-row1">
      //             <div className="form-group col-md-111">
      //               <div className="custom-control custom-checkbox">
      //                 <input
      //                   type="checkbox"
      //                   id="checkbox3"
      //                   className="custom-control-input"
      //                   onChange={this.handledays}
      //                 />
      //                 <label
      //                   className="custom-control-label"
      //                   htmlFor="checkbox3"
      //                 >
      //                   Wednesday
      //                 </label>{" "}
      //               </div>
      //             </div>
      //             <div className="form-group col-md-111">
      //               <span>
      //                 <input type="time" id="opentime" 
      //                 onChange={this.handletime}/>{" "}
      //               </span>
      //             </div>
      //             <div className="form-group col-md-111">
      //               <span>
      //                 <input type="time" id="closetime"
      //                 onChange={this.handletime} />
      //               </span>
      //             </div>
      //           </div>

      //           <div className="form-row1">
      //             <div className="form-group col-md-111">
      //               <div className="custom-control custom-checkbox">
      //                 <input
      //                   type="checkbox"
      //                   id="checkbox4"
      //                   className="custom-control-input"
      //                   onChange={this.handledays}
      //                 />
      //                 <label
      //                   className="custom-control-label"
      //                   htmlFor="checkbox4"
      //                 >
      //                   Thursday
      //                 </label>{" "}
      //               </div>
      //             </div>
      //             <div className="form-group col-md-111">
      //               <span>
      //                 <input type="time" id="opentime"
      //                 onChange={this.handletime} />{" "}
      //               </span>
      //             </div>
      //             <div className="form-group col-md-111">
      //               <span>
      //                 <input type="time" id="closetime" 
      //                 onChange={this.handletime}/>
      //               </span>
      //             </div>
      //           </div>

      //           <div className="form-row1">
      //             <div className="form-group col-md-111">
      //               <div className="custom-control custom-checkbox">
      //                 <input
      //                   type="checkbox"
      //                   id="checkbox5"
      //                   className="custom-control-input"
      //                   onChange={this.handledays}
      //                 />
      //                 <label
      //                   className="custom-control-label"
      //                   htmlFor="checkbox5"
      //                 >
      //                   Friday
      //                 </label>{" "}
      //               </div>
      //             </div>
      //             <div className="form-group col-md-111">
      //               <span>
      //                 <input type="time" id="opentime"
      //                 onChange={this.handletime} />{" "}
      //               </span>
      //             </div>
      //             <div className="form-group col-md-111">
      //               <span>
      //                 <input type="time" id="closetime"
      //                 onChange={this.handletime} />
      //               </span>
      //             </div>
      //           </div>

      //           <div className="form-row1">
      //             <div className="form-group col-md-111">
      //               <div className="custom-control custom-checkbox">
      //                 <input
      //                   type="checkbox"
      //                   id="checkbox6"
      //                   className="custom-control-input"
      //                   onChange={this.handledays}
      //                 />
      //                 <label
      //                   className="custom-control-label"
      //                   htmlFor="checkbox6"
      //                 >
      //                   Saturday
      //                 </label>{" "}
      //               </div>
      //             </div>
      //             <div className="form-group col-md-111">
      //               <span>
      //                 <input type="time" id="opentime"
      //                 onChange={this.handletime} />{" "}
      //               </span>
      //             </div>
      //             <div className="form-group col-md-111">
      //               <span>
      //                 <input type="time" id="closetime"
      //                 onChange={this.handletime} />
      //               </span>
      //             </div>
      //           </div>

      //           <div className="form-row1">
      //             <div className="form-group col-md-111">
      //               <div className="custom-control custom-checkbox">
      //                 <input
      //                   type="checkbox"
      //                   id="checkbox7"
      //                   className="custom-control-input"
      //                   onChange={this.handledays}
      //                 />
      //                 <label
      //                   className="custom-control-label"
      //                   htmlFor="checkbox7"
      //                 >
      //                   Sunday
      //                 </label>{" "}
      //               </div>
      //             </div>
      //             <div className="form-group col-md-111">
      //               <span>
      //                 <input type="time" id="opentime"
      //                 onChange={this.handletime} />{" "}
      //               </span>
      //             </div>
      //             <div className="form-group col-md-111">
      //               <span>
      //                 <input type="time" id="closetime"
      //                 onChange={this.handletime} />
      //               </span>
      //             </div>
      //           </div>
      //         </div>
      //         <div className="form-group">
      //           <div className="custom-control custom-checkbox">
      //             <input
      //               type="checkbox"
      //               className="custom-control-input"
      //               id="userTNC"
      //               defaultChecked={userTNC}
      //               onChange={this.handleUserTNC}
      //             />
      //             <label className="custom-control-label" htmlFor="userTNC">
      //               Accept Terms and Conditions
      //             </label>
      //           </div>
      //           {showErrorUserTNC ? <small className="text-danger mb-0">{registerFormErrorUserTNC}</small> : null}
      //         </div>

      //         {/* <p className="text-danger">{showError ? registerFormError : null}</p> */}
      //         <button
      //           type="submit"
      //           className="btn btn-warning text-uppercase mb-3"
      //           onClick={this.handleCreateAccountBtn}
      //         >
      //           <b>Create an Account</b>
      //         </button>
      //       </form>
      //       {/* ChangeAJP */}
      //       <p className="m-0">
      //         Already have an account?{" "}
      //         <span
      //           className="cursor-pointer text-warning"
      //           onClick={() => this.handleForms()}
      //         >
      //           Login Here
      //         </span>
      //       </p>
      //     </div>
      //   </div>
      //   <Footer />
      // </div>

      // restaurant registration page copy end

      // edit food item

      // <div>
      //   <Modal
      //     size="lg"
      //     show={this.state.showModal}
      //     dialogClassName="modal-90w"
      //     onHide={this.handleClose}
      //   >
      //     {
      //       <Modal.Header closeButton>
      //         <Modal.Title>Edit Menu Item</Modal.Title>
      //       </Modal.Header>
      //     }
      //     <Modal.Body size="lg">
      //       <div className="col-lg-12 col-md-12 col-sm-12 mx-auto">
      //         <form action="javascript:void(0)">
      //           <div
      //             className="form-row"
      //             style={{ display: "flex", flexDirection: "row" }}
      //           >
      //             <div className="col-md-6 col-lg-6">
      //               <div className="form-group col-md-12 col-lg-12">
      //                 <label htmlFor="itemTitle">
      //                   <b>Restaurant Name</b>
      //                 </label>
      //                 <input
      //                   value={userName}
      //                   type="text"
      //                   className="form-control"
      //                   id="itemTitle"
      //                   placeholder="Full name of dish"
      //                   onChange={(e) =>
      //                     this.setState({ userName: e.target.value })
      //                   }
      //                 />
      //               </div>
      //               <div className="form-group col-md-12 col-lg-12">
      //                 <label htmlFor="itemIngredients">
      //                   <b>Email</b>
      //                 </label>
      //                 <input
      //                   value={userEmail}
      //                   type="text"
      //                   className="form-control"
      //                   id="itemIngredients"
      //                   placeholder="Item Ingredients Name"
      //                   onChange={(e) =>
      //                     this.setState({ userEmail: e.target.value })
      //                   }
      //                 />
      //               </div>
      //               <div className="form-group col-md-12">
      //                 <label htmlFor="itemPrice">
      //                   <b>Price</b>
      //                 </label>
      //                 <input
      //                   value={phoneNumber}
      //                   type="number"
      //                   className="form-control"
      //                   id="itemPrice"
      //                   placeholder="Price in number"
      //                   onChange={(e) =>
      //                     this.setState({ phoneNumber: e.target.value })
      //                   }
      //                 />
      //               </div>
      //             </div>
      //             <div
      //               className="col-lg-6 col-md-6 col-6"
      //               style={{ display: "flex", justifyContent: "center" }}
      //             >
      //               <img
      //                 style={{ width: "15em", height: "15em" }}
      //                 alt="Natural Healthy Food"
      //                  src={userProfileImage}
      //               />
      //             </div>
      //             {/* <div className="form-group col-md-6">
      //                               <label className="mb-2"><b>Item Image</b></label>
      //                               <div className="custom-file">
      //                                   <input type="file" className="custom-file-input" id="itemImage" accept="image/*" onChange={this.handleItemImage} />
      //                                   <label value={this.state.itemImage} className="custom-file-label" htmlFor="itemImage">{itemImageLable}</label>
      //                               </div>
      //                           </div> */}
      //           </div>
      //           {/* <div className="col-lg-12 col-md-12 col-12">
      //             <label className="mb-2">
      //               <b>Choose Item Type</b>
      //             </label>
      //             <div className="form-row">
      //               <div className="form-group col-md-3">
      //                 <div className="custom-control custom-radio">
      //                   <input
      //                     type="radio"
      //                     className="custom-control-input"
      //                     id="kebabs"
      //                     value="kebabs"
      //                     name="chooseItemType"
      //                     checked={this.state.chooseItemType === "kebabs"}
      //                     onChange={(e) =>
      //                       this.setState({ chooseItemType: e.target.value })
      //                     }
      //                   />
      //                   <label
      //                     className="custom-control-label"
      //                     htmlFor="kebabs"
      //                   >
      //                     Kebabs
      //                   </label>
      //                 </div>
      //               </div>
      //               <div className="form-group col-md-3">
      //                 <div className="custom-control custom-radio">
      //                   <input
      //                     type="radio"
      //                     className="custom-control-input"
      //                     id="chicken"
      //                     value="chicken"
      //                     name="chooseItemType"
      //                     checked={this.state.chooseItemType === "chicken"}
      //                     onChange={(e) =>
      //                       this.setState({ chooseItemType: e.target.value })
      //                     }
      //                   />
      //                   <label
      //                     className="custom-control-label"
      //                     htmlFor="chicken"
      //                   >
      //                     Chicken
      //                   </label>
      //                 </div>
      //               </div>
      //               <div className="form-group col-md-3">
      //                 <div className="custom-control custom-radio">
      //                   <input
      //                     type="radio"
      //                     className="custom-control-input"
      //                     id="burgers"
      //                     value="burgers"
      //                     name="chooseItemType"
      //                     checked={this.state.chooseItemType === "burgers"}
      //                     onChange={(e) =>
      //                       this.setState({ chooseItemType: e.target.value })
      //                     }
      //                   />
      //                   <label
      //                     className="custom-control-label"
      //                     htmlFor="burgers"
      //                   >
      //                     Burgers
      //                   </label>
      //                 </div>
      //               </div>
      //               <div className="form-group col-md-3">
      //                 <div className="custom-control custom-radio">
      //                   <input
      //                     type="radio"
      //                     className="custom-control-input"
      //                     id="biryani"
      //                     value="biryani"
      //                     name="chooseItemType"
      //                     checked={this.state.chooseItemType === "biryani"}
      //                     onChange={(e) =>
      //                       this.setState({ chooseItemType: e.target.value })
      //                     }
      //                   />
      //                   <label
      //                     className="custom-control-label"
      //                     htmlFor="biryani"
      //                   >
      //                     Biryani
      //                   </label>
      //                 </div>
      //               </div>
      //             </div>

      //             <div className="form-row">
      //               <div className="form-group col-md-3">
      //                 <div className="custom-control custom-radio">
      //                   <input
      //                     type="radio"
      //                     className="custom-control-input"
      //                     id="sauces"
      //                     value="sauces"
      //                     name="chooseItemType"
      //                     checked={this.state.chooseItemType === "sauces"}
      //                     onChange={(e) =>
      //                       this.setState({ chooseItemType: e.target.value })
      //                     }
      //                   />
      //                   <label
      //                     className="custom-control-label"
      //                     htmlFor="sauces"
      //                   >
      //                     Sauces
      //                   </label>
      //                 </div>
      //               </div>
      //               <div className="form-group col-md-3">
      //                 <div className="custom-control custom-radio">
      //                   <input
      //                     type="radio"
      //                     className="custom-control-input"
      //                     id="vegetarian"
      //                     value="vegetarian"
      //                     name="chooseItemType"
      //                     checked={this.state.chooseItemType === "vegetarian"}
      //                     onChange={(e) =>
      //                       this.setState({ chooseItemType: e.target.value })
      //                     }
      //                   />
      //                   <label
      //                     className="custom-control-label"
      //                     htmlFor="vegetarian"
      //                   >
      //                     Vegetarian
      //                   </label>
      //                 </div>
      //               </div>
      //               <div className="form-group col-md-3">
      //                 <div className="custom-control custom-radio">
      //                   <input
      //                     type="radio"
      //                     className="custom-control-input"
      //                     id="bread"
      //                     value="bread"
      //                     name="chooseItemType"
      //                     checked={this.state.chooseItemType === "bread"}
      //                     onChange={(e) =>
      //                       this.setState({ chooseItemType: e.target.value })
      //                     }
      //                   />
      //                   <label className="custom-control-label" htmlFor="bread">
      //                     Bread
      //                   </label>
      //                 </div>
      //               </div>
      //               <div className="form-group col-md-3">
      //                 <div className="custom-control custom-radio">
      //                   <input
      //                     type="radio"
      //                     className="custom-control-input"
      //                     id="specials"
      //                     value="specials"
      //                     name="chooseItemType"
      //                     checked={this.state.chooseItemType === "specials"}
      //                     onChange={(e) =>
      //                       this.setState({ chooseItemType: e.target.value })
      //                     }
      //                   />
      //                   <label
      //                     className="custom-control-label"
      //                     htmlFor="specials"
      //                   >
      //                     Specials
      //                   </label>
      //                 </div>
      //               </div>
      //             </div>
      //             {showError ? (
      //               <p className="text-danger">{registerFormError}</p>
      //             ) : null}
      //           </div> */}
      //           <div className="col-lg-12 col-md-12 col-sm-12">
      //             <button
      //               type="submit"
      //               className="btn btn-warning text-uppercase mb-3"
      //               onClick={this.handleUpdateBtn}
      //             >
      //               <b>Save</b>
      //             </button>
      //             {"  "}
      //             <button
      //               type="submit"
      //               className="btn btn-warning text-uppercase mb-3"
      //               onClick={(e) => this.handleDeleteBtn}
      //             >
      //               <b>Clear</b>
      //             </button>
      //           </div>
      //           <div className="col-lg-12 col-md-12 col-sm-12"></div>
      //         </form>
      //       </div>
      //     </Modal.Body>
      //   </Modal>
      // </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};
export default connect(mapStateToProps, null)(RestaurantProfile);
