import React, { Component } from "react";
import Modal from "react-bootstrap/Modal";
import { connect } from "react-redux";
import { restaurant_list } from "../ServiceLayer/Restaurant";
import RegisterRestaurant from "../screens/RegisterRestaurant";
import Button from "react-bootstrap/Button";
import Swal from "sweetalert2";
import axios from "axios";
import { edituserdata, deleteUser } from "../ServiceLayer/User";

import "bootstrap/dist/css/bootstrap.css";

import "../App.css";
import { checkUtils } from "@material-ui/pickers/_shared/hooks/useUtils";
import { faLessThanEqual } from "@fortawesome/free-solid-svg-icons";

class UpdateRestaurantProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isdelete: true,
    };
    this.handleUpdateBtn = this.handleUpdateBtn.bind(this);
    this.handlepushbtn = this.handlepushbtn.bind(this);
    this.handleClearBt = this.handleClearBt.bind(this);
    this.component();
  }
  // getiing the restaurant data
  async component() {
    let user = {};
    var token = localStorage.getItem("securityToken");
    const options = {
      url: process.env.REACT_APP_NodeJS_ServiceURL + `/api/v1/auth/me`,
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json;charset=UTF-8",
        Authorization: `Bearer ${token}`,
      },
    };
    await axios(options)
      .then((response) => {
        console.log("dta", response.data.data);
        user = response.data.data;
        console.log("menuitem id", user.id);
      })
      .catch((error) => {
        console.log(error);
      });
    this.setState({
      userName: user.userName,
      phoneNumber: user.phoneNumber,
      userEmail: user.userEmail,
      userPincode: user.userPincode,
      userAddress: user.userAddress,
      chooseservice: user.chooseservice,
      userCity: user.userCity,
      userCountry: user.userCountry,
      userProfileImageUrl: user.userProfileImageUrl,
      showModal: this.props.showModal,
      user: user,
    });
  }

  // handle button for updating the user data
  async handleUpdateBtn() {
    const {
      userName,
      userEmail,
      userAddress,
      userProfileImageUrl,
      phoneNumber,
      userCity,
      userCountry,
      chooseservice,
      userPincode,
     
    } = this.state;

    const updateRestaurant = {
      userName,
      userEmail,
      userAddress,
      userProfileImageUrl,
      phoneNumber,
      chooseservice,
      userPincode,
      userCity,
      userCountry,
      
      propsHistory: this.props.history,
    };
    try {
      const updateRestaurantReturn = await edituserdata(updateRestaurant);
      Swal.fire({
        title: "Success",
        text: "User Successfully Updated",
        type: "success",
      });
    } catch (error) {
      console.log(error);
    }

    console.log();
  }

  async handleDeleteBtn(e) {
    console.log(e);
    const id = e;
    Swal.fire({
      title: "Delete Menu Item?",
      text: "Are you sure that you want to delete this item?",
      icon: "question",
      dangerMode: true,
    }).then(async (result) => {
      try {
        // call the delete api
        await deleteUser(id);


        Swal.fire({
          title: "Success",
          text: "The User has been deleted!!",
          type: "success",
        }).then(() => {        
          window.location.reload(false);
        });
      } catch (error) {
        Swal.fire({
          title: "Error",
          text: error,
          type: "error",
        });
      }
    });
  }
  handleClearBt(){
    this.setState({
        userName: "",
        phoneNumber: "",
        userEmail: "",
        userPincode: "",
        userAddress: "",
        userProfileImageUrl: "",
        chooseservice: [],
        userCity: "",
      userCountry: "",
        


    })

  }
  handlepushbtn(){
    this.setState({
      isdelete : false
    })

  }
  //Close the model
  handleClose = () => this.props.handleClose(false);

checktype(service)
{
  const { chooseservice} = this.state;
  var bool;
  console.log("chooseservice", chooseservice);
  if (chooseservice) {
      chooseservice.map((val) => {
      if (val === service) {
        console.log("inside if", val === service);
        bool = true;
      }
    });
    if (bool === true)
    {
      console.log("bool: ",bool);
      return true;
      }
  }
  }
  
  render() {
    const { showError, registerFormError, isdelete,b,l,d } = this.state;
    //const history = this.props.history;
    return (
      <div>
        <Modal
          size="lg"
          show={this.state.showModal}
          dialogClassName="modal-90w"
          onHide={this.handleClose}
        >
          {
            <Modal.Header closeButton>
              <Modal.Title>Restaurant Details</Modal.Title>
            </Modal.Header>
          }
          <Modal.Body size="lg">
            <div className="col-lg-12 col-md-12 col-sm-12 mx-auto text-justify">
              <form action="javascript:void(0)">
                <div
                  className="form-row"
                  style={{ display: "flex", flexDirection: "row" }}
                >
                  <div className="col-md-6 col-lg-6">
                    <div className="form-group col-md-12 col-lg-12">
                      <label htmlFor="Username">
                        <b>Username</b>
                      </label>
                      <input
                        value={this.state.userName}
                        type="text"
                        className="form-control"
                        id="Username"
                        placeholder="Username"
                        onChange={(e) =>
                          this.setState({ userName: e.target.value })
                        }
                      />
                    </div>
                    <div className="form-group col-md-12 col-lg-12">
                      <label htmlFor="email">
                        <b>Email</b>
                      </label>
                      <input
                        value={this.state.userEmail}
                        type="email"
                        className="form-control"
                        id="email"
                        placeholder="Email"
                        onChange={(e) =>
                          this.setState({ userEmail: e.target.value })
                        }
                      />
                    </div>
                    <div className="form-group col-md-12">
                      <label htmlFor="password">
                        <b>Phone Number</b>
                      </label>
                      <input
                        value={this.state.phoneNumber}
                        type="text"
                        className="form-control"
                        id="phoneNumber"
                        placeholder="Phone Number"
                        onChange={(e) =>
                          this.setState({ phoneNumber: e.target.value })
                        }
                      />
                    </div>
                    <div className="form-group col-md-12">
                      <label htmlFor="password">
                        <b>Address</b>
                      </label>
                      <input
                        value={this.state.userAddress}
                        type="text"
                        className="form-control"
                        id="address"
                        placeholder="address"
                        onChange={(e) =>
                          this.setState({ userAddress: e.target.value })
                        }
                      />
                      </div>
                      <div className="form-group col-md-12">
                      <label htmlFor="password">
                        <b>Country</b>
                      </label>
                      <input
                        value={this.state.userCountry}
                        type="text"
                        className="form-control"
                        id="country"
                        placeholder="country"
                        onChange={(e) =>
                          this.setState({ userCountry: e.target.value })
                        }
                      />
                      </div>
                      <div className="form-group col-md-12">
                      <label htmlFor="password">
                        <b>City</b>
                      </label>
                      <input
                        value={this.state.userCity}
                        type="text"
                        className="form-control"
                        id="city"
                        placeholder="City"
                        onChange={(e) =>
                          this.setState({ userCity: e.target.value })
                        }
                      />
                      </div>
                      <div className="form-group col-md-12">
                      <label htmlFor="password">
                        <b>Pincode</b>
                      </label>
                      <input
                        value={this.state.userPincode}
                        type="number"
                        className="form-control"
                        id="pincode"
                        placeholder="pincode"
                        onChange={(e) =>
                          this.setState({ userPincode: e.target.value })
                        }
                      />
                    </div>
                    <label><b>Services</b></label>
                    <div className="form-row">
                      <div className="form-group col">
                       
                        <div className="custom-control custom-radio">
                          <input
                            type="checkbox"
                            className="custom-control-input"
                            id="Breakfast"
                            value="Breakfast"
                            checked={this.checktype("Breakfast")}
                            name="Breakfast"
                            onChange={(e)=>
                              this.state.chooseservice.push(e.target.value)}                          />
                          <label className="custom-control-label" htmlFor="Breakfast">
                            Breakfast
                          </label>
                        </div>
                        <div className="custom-control custom-radio">
                          {console.log("this.state.chooseservice[1]",this.state.chooseservice)}
                          <input
                            type="checkbox"
                            className="custom-control-input"
                            id="Lunch"
                            value="Lunch"
                            
                            name="Lunch"
                            checked={this.checktype("Lunch")}
                             onChange={(e) =>
                              this.state.chooseservice.push(e.target.value)}   
                          />
                          <label className="custom-control-label" htmlFor="Lunch">
                            Lunch
                          </label>
                        </div>
                        
                        <div className="custom-control custom-radio">
                          <input
                            type="checkbox"
                            className="custom-control-input"
                            id=" Dinner"
                            value="Dinner"
                            name=" Dinner"
                            checked = {this.checktype("Dinner")}
                           onChange={(e) =>
                              this.state.chooseservice.push(e.target.value)}   
                          />
                          <label className="custom-control-label" htmlFor=" Dinner">
                            Dinner
                          </label>
                        </div>
                        <div className="custom-control custom-radio">
                          <input
                            type="checkbox"
                            className="custom-control-input"
                            id="Desserts"
                            value="Desserts"
                            name="Desserts"
                            checked = {this.checktype("Desserts")}
                             onChange={(e) =>
                              this.state.chooseservice.push(e.target.value)}   
                          />
                          <label className="custom-control-label" htmlFor="Desserts">
                            Desserts
                          </label>
                        </div>
                        <div className="custom-control custom-radio">
                          <input
                            type="checkbox"
                            className="custom-control-input"
                            id="Drinks"
                            value="Drinks"
                            name="Drinks"
                            checked = {this.checktype("Drinks")}
                           onChange={(e) =>
                              this.state.chooseservice.push(e.target.value)}   
                          />
                          <label className="custom-control-label" htmlFor="Drinks">
                            Drinks
                          </label>
                        </div>
                        <div className="custom-control custom-radio">
                          <input
                            type="checkbox"
                            className="custom-control-input"
                            id="Specials"
                            value="Specials"
                            name="Specials"
                            checked = {this.checktype("Specials")}
                            onChange={(e) =>
                              this.state.chooseservice.push(e.target.value)}   
                          />
                          <label className="custom-control-label" htmlFor="Specials">
                            Specials
                          </label>
                        </div>
                </div>
             </div>
                    {console.log("chooseservice",this.state.chooseservice)}
                  </div>
                  <div
                    className="col-lg-6 col-md-6 col-6"
                    style={{ display: "flex", justifyContent: "center" }}
                  >
                    <img
                      style={{ width: "15em", height: "15em" }}
                      alt="Natural Healthy Food"
                      src={this.state.userProfileImageUrl}
                    />
                    {/* <input
                      type="file"
                      className="custom-file-input"
                      id="itemImage"
                      accept="image/*"
                      onChange={(e) =>
                        this.setState({ userProfileImageUrl: e.target.value })
                      }
                    />
                    <label
                      value={this.state.userProfileImageUrl}
                      className="custom-file-label"
                      htmlFor="itemImage"
                    ></label> */}
                  </div>
                  {/* <div className="form-group col-md-6">
                                    <label className="mb-2"><b>Item Image</b></label>
                                    <div className="custom-file">
                                        <input type="file" className="custom-file-input" id="itemImage" accept="image/*" onChange={this.handleItemImage} />
                                        <label value={this.state.itemImage} className="custom-file-label" htmlFor="itemImage">{itemImageLable}</label>
                                    </div>
                                </div> */}
                </div>       
               {isdelete ?
               (<div className="col-lg-12 col-md-12 col-sm-12">
                 {" "}
                  <button
                    type="submit"
                    className="btn btn-warning text-uppercase mb-3"
                    onClick={this.handlepushbtn}
                  >
                    <b>Update</b>
                  </button>
                  {"  "}
                  <button
                    type="submit"
                    className="btn btn-warning text-uppercase mb-3"
                    onClick={(e) => this.handleDeleteBtn(this.state.user._id)}
                  >
                    <b>Delete</b>
                  </button>
                </div>) : 
                (<div className="col-lg-12 col-md-12 col-sm-12">
                 {" "}
                  <button
                    type="submit"
                    className="btn btn-warning text-uppercase mb-3"
                    onClick={this.handleUpdateBtn}
                  >
                    <b>Save</b>
                  </button>
                  {"  "}
                  <button
                    type="submit"
                    className="btn btn-warning text-uppercase mb-3"
                    onClick={this.handleClearBt}
                  >
                    <b>Clear</b>
                  </button>
                </div>) }
               
              </form>
            </div>
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
  console.log("dcfevcdvcdcdcd", state);
};
// const mapDispatchToProps = (dispatch) => {
//   return {
//     update_user: () => dispatch(update_user()),
//   };
// };
export default connect(mapStateToProps, null)(UpdateRestaurantProfile);
