import React, { Component, useState } from "react";
import StarRating from "react-bootstrap-star-rating";
import { FaStar } from "react-icons/fa";
// import Navbar from '../components/Navbar';
import Navbar2 from "../components/Navbar2";
import Footer from "../components/Footer";
//import firebase from '../config/firebase';
import axios from "axios";
import { connect } from "react-redux";
// import { orderNow } from '../config/firebase';
import { orderNow } from "../ServiceLayer/Order";
import Swal from "sweetalert2";
import "bootstrap/dist/css/bootstrap.css";
import "../App.css";
import {ReviewDetails} from "../ServiceLayer/Restaurant";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class RestaurantDetails extends Component {
  constructor() {
    super();

    this.state = {
      // Tab 1 displays all the menu items. Tab 1 is active initially when the page renders
      // Tab 2 displays reviews and ratings
      // Tab 3 displays the restaurant information
      tab1: "col-12 col-lg-4 col-md-4 text-center res-details-tab-active",
      tab2: "col-12 col-lg-4 col-md-4 text-center",
      tab3: "col-12 col-lg-4 col-md-4 text-center",
      tab1Content: true,
      tab2Content: false,
      tab3Content: false,
      Quantity: [1,2,3],
      //Cart items and total price are all null initially
      cartItemsList: [],
      totalPrice: 0,
      showCartList: false,
      count: 1,
      //Review, rating and error message is null initially
      rating: null,
      review: null,
      userName: "",
      showErrorReview: false,
      reviewErrorMessage: null,
      //Render menu item list
      renderMenuItemsList: true,
      //Render categorized menu item list
      renderItemCategories: false,
      category: "",
      renderSearchMenuItems: false,
    };
    this.handleItemCategories = this.handleItemCategories.bind(this);
    this.handledescription = this.handledescription.bind(this);
    this.handleSearchBar = this.handleSearchBar.bind(this);
    this.handleReviewForm = this.handleReviewForm.bind(this);
  }

  async componentDidMount() {
    const { state } = await this.props.location;
    // console.log("compount details", state);
    this.fetchMenuItems();
    if (state) {
      this.setState({
        resDetails: state,
      });
      this.handleSearchBar(state);
    } else {
      this.props.history.push("/restaurants");
    }
  }

  static getDerivedStateFromProps(props) {
    const { state } = props.location;
    const { user } = props;
    //console.log("derived state", state);
    //console.log("derived user", user);
    return {
      resDetails: state,
      userDetails: user,
    };
  }

  handleTabs(e) {
    if (e === "tab1") {
      this.setState({
        tab1: "col-12 col-lg-4 col-md-4 text-center res-details-tab-active",
        tab2: "col-12 col-lg-4 col-md-4 text-center",
        tab3: "col-12 col-lg-4 col-md-4 text-center",
        tab1Content: true,
        tab2Content: false,
        tab3Content: false,
      });
    } else if (e === "tab2") {
      this.setState({
        tab1: "col-12 col-lg-4 col-md-4 text-center",
        tab2: "col-12 col-lg-4 col-md-4 text-center res-details-tab-active",
        tab3: "col-12 col-lg-4 col-md-4 text-center",
        tab1Content: false,
        tab2Content: true,
        tab3Content: false,
      });
    } else if (e === "tab3") {
      this.setState({
        tab1: "col-12 col-lg-4 col-md-4 text-center",
        tab2: "col-12 col-lg-4 col-md-4 text-center",
        tab3: "col-12 col-lg-4 col-md-4 text-center res-details-tab-active",
        tab1Content: false,
        tab2Content: false,
        tab3Content: true,
      });
    }
  }
  // method to get loged in user data

  fetchUserData() {
    const username = mapStateToProps({ user: this.state }).user.userDetails
      .userName;

    console.log("user", username);
  }

  fetchMenuItems() {
    const { resDetails } = this.state;
    var menuItemsList = [];
    const options = {
      url:
        process.env.REACT_APP_NodeJS_ServiceURL +
        "/api/v1/menuitems/" +
        resDetails.id,
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json;charset=UTF-8",
      },
    };
    axios(options)
      .then((response) => {
        console.log(response.data.data);
        menuItemsList = response.data.data;
        menuItemsList.forEach((doc) => {
          doc.id = doc._id;
        });
        this.setState({
          menuItemsList: menuItemsList,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }
  //function to add items to the cart
  addToCart(item) {
    const { cartItemsList, totalPrice, count } = this.state;

    console.log("count cart before", count);
    if (item) {
      cartItemsList.push(item);
      this.setState({
        totalPrice: totalPrice + Number(item.itemPrice),
        cartItemsList: cartItemsList,
        showCartList: true,
      });
      console.log("count cart after", count);
    }
  }
  //function to add quantity
  addQuantity(item) {
    console.log("Add Quantity");
    const { cartItemsList, totalPrice, count } = this.state;
    console.log("before count", count);
    if (item) {
      // cartItemsList.push(item);
      this.setState({
        count: count + 1,

        totalPrice: Number(item.itemPrice) * (count + 1),
      });
      console.log("after count", count);
    }
  }
  //function to remove items from cart
  removeCartItem(itemIndex) {
    const { cartItemsList, totalPrice } = this.state;
    const removedItemPrice = Number(cartItemsList[itemIndex].itemPrice);
    cartItemsList.splice(itemIndex, 1);
    this.setState({
      totalPrice: totalPrice - removedItemPrice,
      cartItemsList: cartItemsList,

    });
  }
  //function to remove quantity
  removeQuantity(itemIndex) {
    console.log("Remove Quantity");
    const { totalPrice, cartItemsList } = this.state;
    const removedquantity = Number(cartItemsList[itemIndex].itemPrice);
    const count = 1;
    this.setState({
      count: count - 1,
      totalPrice: totalPrice - removedquantity,
    });
    console.log("count", count);
  }
  // On click on Confirm Order button
  async handleConfirmOrderBtn() {
    const {
      cartItemsList,
      time,
      totalPrice,
      resDetails,
      userDetails,
      Quantity,
    } = this.state;
    //console.log(cartItemsList.length)
    if (userDetails) {
      if (!userDetails.isRestaurant) {
        if (cartItemsList.length > 0) {
          try {
            const history = this.props.history;
            const time = new Date().toLocaleString();
            const orderNowReturn = await orderNow(
              cartItemsList,
              time,
              totalPrice,
              resDetails,
              userDetails,
              history,
              Quantity,
            );
            //console.log(orderNowReturn)
            // console.log("Successfully Ordered")
            /*Swal.fire({
              title: "Success",
              text: "Successfully Ordered",
              type: "success",
            }).then(() => {
              history.push("/shipping-details");
            });*/
            this.props.history.push("/shipping-details");
          } catch (error) {
            // console.log(" Error in confirm order => ", error)
            Swal.fire({
              title: "Error",
              text: error,
              type: "error",
            });
          }
        } else {
          console.log("You have to select atleast one item");
          Swal.fire({
            title: "Error",
            text: "You have to select atleast one item",
            type: "error",
          });
        }
      } else {
        // console.log("You are not able to order")
        Swal.fire({
          title: "Error",
          text: "You are not able to order",
          type: "error",
        });
      }
    } else {
      // console.log("You must be Loged In")
      Swal.fire({
        title: "Error",
        text: "You must be Loged In",
        type: "error",
      }).then(() => {
        this.props.history.push("/login");
      });
    }
  }
  // Rendering the menu items list
  _renderMenuItemsList() {
    const { menuItemsList, count } = this.state;
    if (menuItemsList) {
      return Object.keys(menuItemsList).map((val) => {
        return (
          <div
            className="container border-bottom pb-2 px-lg-0 px-md-0 mb-4"
            key={menuItemsList[val].id}
          >
            <div className="row">
              <div className="col-lg-2 col-md-3 col-8 offset-2 offset-lg-0 offset-md-0 px-0 mb-3 text-center">
                <img
                  style={{ width: "70px", height: "70px" }}
                  alt="Natural Healthy Food"
                  src={menuItemsList[val].itemImageUrl}
                />
              </div>
              <div className="col-lg-7 col-md-6 col-sm-12 px-0">
                <h6 className="">{menuItemsList[val].itemTitle}</h6>
                <p className="">
                  <small>{menuItemsList[val].itemIngredients}</small>
                </p>
              </div>
              <div className="col-lg-3 col-md-3 col-sm-12 px-0 text-center">
                <span className="mx-3">RS.{menuItemsList[val].itemPrice}</span>
                <span
                  className="menuItemsListAddBtn"
                  onClick={() => this.addToCart(menuItemsList[val])}
                >
                  <FontAwesomeIcon icon="plus" className="text-warning" />
                </span>
              </div>
            </div>
          </div>
        );
      });
    }
  }
  // Rendering cart items list
  _renderCartItemsList() {
    const { count, cartItemsList, menuItemsList } = this.state;
    if (cartItemsList) {
      return Object.keys(cartItemsList).map((val) => {
        return (
          <li className="food-item border-bottom pb-2 mb-3" key={val}>
            <div className="row">
              <div className="col-8 pr-0">
                <p className="mb-0">
                  {cartItemsList[val].itemTitle} ({count})
                </p>
                <span
                  className="menuQuantityBtn"
                  onClick={() => this.addQuantity(menuItemsList[val])}
                >
                  <svg
                    width="1em"
                    height="1em"
                    viewBox="0 0 16 16"
                    class="bi bi-plus"
                    fill="currentColor"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      fill-rule="evenodd"
                      d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"
                    />
                  </svg>
                </span>

                <span
                  className="menuQuantityBtn"
                  onClick={() => this.removeQuantity(val)}
                >
                  <svg
                    width="1em"
                    height="1em"
                    viewBox="0 0 16 16"
                    class="bi bi-dash"
                    fill="currentColor"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      fill-rule="evenodd"
                      d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z"
                    />
                  </svg>
                </span>
              </div>
              <div className="col-4 pl-0 text-right">
                <p className="mb-0">
                  <span className="food-price">
                    RS.{cartItemsList[val].itemPrice} {/*Your order price  */}
                  </span>

                  <span
                    onClick={() => this.removeCartItem(val)}
                    className="remove-food-item"
                  >
                    <FontAwesomeIcon icon="times" />
                  </span>
                </p>
              </div>
            </div>
          </li>
        );
      });
    }
  }
  //Handle description method
  handledescription(e) {
    const review = e;
    if (review == null) {
      this.setState({
        showErrorReview: true,
        reviewErrorMessage: "Description is required",
        review: "",
      });
    } else {
      this.setState({
        showErrorReview: false,
        reviewErrorMessage: "",
        review: review,
      });
    }
  }
  // review functionality method
  async handleReviewForm() {
    const { rating, review, resDetails, userDetails } = this.state;
    
    //console.log(cartItemsList.length)
    if (userDetails) {
      if (!userDetails.isRestaurant) {
        if (review != null) {
          try {
            const history = this.props.history;
            const reviewReturn = await ReviewDetails(
              rating,
              review,
              resDetails,
              userDetails,
              history
            );
            //console.log(orderNowReturn)
            // console.log("Successfully Ordered")
            Swal.fire({
              title: "Success",
              text: "Successfully Review",
              type: "success",
            }).then(() => {
              history.push("/");
            });
          } catch (error) {
            // console.log(" Error in confirm order => ", error)
            Swal.fire({
              title: "Error",
              text: "please Login as a customer",
              type: "error",
            });
            
          }
        }
        
        else {
          Swal.fire({
            title: "Error",
            text: "Description is required",
            type: "error",
          });
        }
      } else {
        Swal.fire({
          title: "Error",
          text: "only customer can give reviews",
          type: "error",
        });
      }
    } else {
      // console.log("You must be Loged In")
      Swal.fire({
        title: "Error",
        text: "You must be Loged In",
        type: "error",
      }).then(() => {
        this.props.history.push("/login");
      });
    }
  }
  // Handle for category menu items
  handleItemCategories(event) {
    const target = event.target;
    const name = target.name.toLocaleLowerCase(); // Name of the item selected in the categories
    //console.log("Categorize items", name.toLocaleLowerCase());
    this.setState({
      category: name,
      renderMenuItemsList: false,
      renderItemCategories: true,
    });
    this._renderCartItemsList();
  }
  // render list of category item
  _renderItemCategories() {
    const { menuItemsList } = this.state;
    const { category } = this.state;
    //console.log("menuItemsList", menuItemsList);
    //console.log("category", category);
    //const name = event.target.name.toLocaleLowerCase();
    if (menuItemsList) {
      return Object.keys(menuItemsList).map((val) => {
        //console.log("menuItemsList[val].chooseItemType : ", menuItemsList[val].chooseItemType[0]);
        if (
          category === menuItemsList[val].chooseItemType[0].toLocaleLowerCase()
        ) {
          return (
            <div
              className="container border-bottom pb-2 px-lg-0 px-md-0 mb-4"
              key={menuItemsList[val].id}
            >
              <div className="row">
                <div className="col-lg-2 col-md-3 col-8 offset-2 offset-lg-0 offset-md-0 px-0 mb-3 text-center">
                  <img
                    style={{ width: "70px", height: "70px" }}
                    alt="Natural Healthy Food"
                    src={menuItemsList[val].itemImageUrl}
                  />
                </div>
                <div className="col-lg-7 col-md-6 col-sm-12 px-0">
                  <h6 className="">{menuItemsList[val].itemTitle}</h6>
                  <p className="">
                    <small>{menuItemsList[val].itemIngredients}</small>
                  </p>
                </div>
                <div className="col-lg-3 col-md-3 col-sm-12 px-0 text-center">
                  <span className="mx-3">
                    RS.{menuItemsList[val].itemPrice}
                  </span>
                  <span
                    className="menuItemsListAddBtn"
                    onClick={() => this.addToCart(menuItemsList[val])}
                  >
                    <FontAwesomeIcon icon="plus" className="text-warning" />
                  </span>
                </div>
              </div>
            </div>
          );
        }
      });
    }
  }
  // Handle for search menu items
  handleSearchBar(event) {
    const searchText = event;
    //console.log("searchtext", searchText);
    const { menuItemsList } = this.state;
    //console.log("menuItemsList", menuItemsList);
    if (menuItemsList) {
      Object.keys(menuItemsList).map((val) => {});
      const result = menuItemsList.filter((val) => {
        return (
          val.itemTitle
            .toLocaleLowerCase()
            .indexOf(searchText.toLocaleLowerCase()) !== -1
        );
      });
      if (searchText.length > 0) {
        this.setState({
          renderMenuItemsList: false,
          renderItemCategories: false,
          renderSearchMenuItems: true,
          searchMenuItems: result,
          searchText: searchText,
        });
      } else {
        this.setState({
          renderMenuItemsList: true,
          renderItemCategories: false,
          renderSearchMenuItems: false,
          searchMenuItems: result,
          searchText: searchText,
        });
      }
    }
  }
  // render the search item list
  _renderSearchMenuItems() {
    const { searchText, searchMenuItems } = this.state;
    if (searchMenuItems) {
      return Object.keys(searchMenuItems).map((val) => {
        return (
          <div
            className="container border-bottom pb-2 px-lg-0 px-md-0 mb-4"
            key={searchMenuItems[val].id}
          >
            <div className="row">
              <div className="col-lg-2 col-md-3 col-8 offset-2 offset-lg-0 offset-md-0 px-0 mb-3 text-center">
                <img
                  style={{ width: "70px", height: "70px" }}
                  alt="Natural Healthy Food"
                  src={searchMenuItems[val].itemImageUrl}
                />
              </div>
              <div className="col-lg-7 col-md-6 col-sm-12 px-0">
                <h6 className="">{searchMenuItems[val].itemTitle}</h6>
                <p className="">
                  <small>{searchMenuItems[val].itemIngredients}</small>
                </p>
              </div>
              <div className="col-lg-3 col-md-3 col-sm-12 px-0 text-center">
                <span className="mx-3">
                  RS.{searchMenuItems[val].itemPrice}
                </span>
                <span
                  className="menuItemsListAddBtn"
                  onClick={() => this.addToCart(searchMenuItems[val])}
                >
                  <FontAwesomeIcon icon="plus" className="text-warning" />
                </span>
              </div>
            </div>
          </div>
        );
      });
    }
  }

  render() {
    const {
      tab1,
      tab2,
      tab3,
      tab1Content,
      tab2Content,
      tab3Content,
      resDetails,
      totalPrice,
      cartItemsList,
      showCartList,
      renderMenuItemsList,
      renderItemCategories,
      renderSearchMenuItems,
      rating,
      count,
      review,
      showErrorReview,
      reviewErrorMessage,
    } = this.state;
    return (
      <div>
        <div className="container-fluid res-details-cont1">
          <div className="">
            {/* <Navbar history={this.props.history} /> */}
            <Navbar2 history={this.props.history} />
            <div className="container px-0 res-details-cont1-text mx-0">
              <div className="container">
                <div className="row">
                  <div className="col-lg-2 col-md-3 col-6 text-lg-center text-md-center pr-0 mb-2">
                    <img
                      className="p-2 bg-white rounded text-center"
                      alt="Natural Healthy Food"
                      style={{ width: "60%" }}
                      src={resDetails.userProfileImageUrl}
                    />
                  </div>
                  <div className="col-lg-10 col-md-9 col-12 pl-lg-0 pl-md-0">
                    <h1 className="restaurant-title">{resDetails.userName}</h1>
                    <p className="restaurant-text">
                      {resDetails.typeOfFood.join(", ")}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div style={{ background: "#EBEDF3" }} className="container-fluid py-5">
          <div className="container">
            <div className="row">
              <div className="col-lg-2 col-md-2 col-sm-12">
                <div className="listing-category">
                  <div className="category-heading py-0 mb-1">
                    <h6 className="m-0">
                      <FontAwesomeIcon icon="utensils" className="mr-2" />
                      Categories
                    </h6>
                  </div>
                  <div>
                    <ul className="category-list">
                      <li>
                        <a
                          name="Kebabs"
                          type="onClick"
                          onClick={this.handleItemCategories}
                        >
                          Kebabs
                        </a>
                      </li>
                      <li>
                        <a
                          name="Chicken"
                          type="onClick"
                          onClick={this.handleItemCategories}
                        >
                          Chicken
                        </a>
                      </li>
                      <li>
                        <a
                          name="Burgers"
                          type="onClick"
                          onClick={this.handleItemCategories}
                        >
                          Burgers
                        </a>
                      </li>
                      <li>
                        <a
                          name="Biryani"
                          type="onClick"
                          onClick={this.handleItemCategories}
                        >
                          Biryani
                        </a>
                      </li>
                      <li>
                        <a
                          name="Sauces"
                          type="onClick"
                          onClick={this.handleItemCategories}
                        >
                          Sauces
                        </a>
                      </li>
                      <li>
                        <a
                          name="Vegetarian"
                          type="onClick"
                          onClick={this.handleItemCategories}
                        >
                          Vegetarian
                        </a>
                      </li>
                      <li>
                        <a
                          name="Bread"
                          type="onClick"
                          onClick={this.handleItemCategories}
                        >
                          Bread
                        </a>
                      </li>
                      <li>
                        <a
                          name="Specials"
                          type="onClick"
                          onClick={this.handleItemCategories}
                        >
                          Specials
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="col-lg-7 col-md-7 col-sm-12">
                <div className="container">
                  <div className="row">
                    <div
                      className={tab1}
                      onClick={() => this.handleTabs("tab1")}
                    >
                      <p className="res-details-tab-text">
                        <FontAwesomeIcon
                          icon="concierge-bell"
                          className="mr-3"
                        />
                        Menu
                      </p>
                    </div>
                    <div
                      className={tab2}
                      onClick={() => this.handleTabs("tab2")}
                    >
                      <p className="res-details-tab-text">
                        <FontAwesomeIcon icon="comment-alt" className="mr-3" />
                        Reviews
                      </p>
                    </div>
                    <div
                      className={tab3}
                      onClick={() => this.handleTabs("tab3")}
                    >
                      <p className="res-details-tab-text">
                        <FontAwesomeIcon icon="info-circle" className="mr-3" />
                        Restaurant Info
                      </p>
                    </div>
                  </div>
                  {tab1Content && (
                    <div className="row menu-section">
                      <div className="col-12 bg-white p-4">
                        <div className="input-group input-group-sm mb-4 mt-2">
                          <input
                            type="text"
                            className="form-control search-menu-input"
                            htmlFor="search-menu"
                            placeholder="Search food item"
                            onChange={(e) =>
                              this.handleSearchBar(e.target.value)
                            }
                          />
                          <div className="input-group-append">
                            <span
                              className="input-group-text search-menu-text"
                              id="search-menu"
                            >
                              <FontAwesomeIcon icon="search" />
                            </span>
                          </div>
                        </div>
                        <div>
                          <h6 className="mb-4 text-warning">
                            Best food items:
                          </h6>
                          {renderSearchMenuItems &&
                            this._renderSearchMenuItems()}
                          {renderMenuItemsList && this._renderMenuItemsList()}
                          {renderItemCategories && this._renderItemCategories()}
                        </div>
                      </div>
                    </div>
                  )}
                  {tab2Content && (
                    <div className="row review-section">
                      <div className="col-12 bg-white p-4">
                        <h5>Customer Reviews For {resDetails.userName}</h5>
                        <div className="row p-3">
                          <div className="col-3 text-left mt-5">
                            <img
                              alt="Review Icon"
                              src={require("../assets/images/icon-review.png")}
                            />
                          </div>
                          <div className="col-9 pl-0">
                            <p className="">
                              <strong>Write your own reviews</strong>
                            </p>
                            <textarea
                              className="form-control"
                              rows="4"
                              name="review"
                              placeholder="Your Review..."
                              onKeyUp={(e) =>
                                this.handledescription(e.target.value)
                              }
                            ></textarea>
                            <small className="text-danger">
                              {showErrorReview ? reviewErrorMessage : null}
                            </small>
                            <div>
                              {[...Array(5)].map((star, i) => {
                                const ratingValue = i + 1;
                                return (
                                  <label>
                                    <input
                                      className="star-rating-component"
                                      type="radio"
                                      name="rating"
                                      value={ratingValue}
                                      onClick={(e) =>
                                        this.setState({
                                          rating: e.target.value,
                                        })
                                      }
                                    />
                                    <FaStar
                                      className="star"
                                      color={
                                        ratingValue <= rating
                                          ? "#ffc107"
                                          : "#e4e5e9"
                                      }
                                      size={30}
                                    />
                                  </label>
                                );
                              })}
                            </div>
                            <div className="col-9 pl-0">
                              <button
                                type="submit"
                                className="btn btn-sm btn-success"
                                onClick={this.handleReviewForm}
                              >
                                Submit
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                  {tab3Content && (
                    <div className="row info-section">
                      <div className="col-12 bg-white p-4">
                        <h4>Overview: {resDetails.userName}</h4>
                        <p>
                          Base prepared fresh daily. Extra toppings are
                          available in choose extra Choose you sauce: Go for BBQ
                          sauce or piri piri sauce on your pizza base for no
                          extra cost. Choose your cut: Triangular, square,
                          fingers or Un cut on any size pizza
                        </p>
                        <div class="text-justify">
                          <div class="row">
                            <div class="col">
                              <h6>Email:</h6>
                              {resDetails.userEmail}
                            </div>
                            <div class="col">
                              <h6>Phone number:</h6>
                              {resDetails.phoneNumber}
                            </div>
                            <div class="col">
                              <h6>Address:</h6>
                              {resDetails.userAddress}, {resDetails.userCity},{" "}
                              {resDetails.userCountry}, {resDetails.userPincode}
                            </div>
                          </div>
                          <div class="row">
                            <div class="col">
                              <h6>Services:</h6>
                              {resDetails.chooseservice.join(", ")}
                              {console.log(
                                "chooseservice",
                                resDetails.chooseservice
                              )}
                              {/* <h6>Time:{resDetails.time}</h6> */}
                            </div>
                            <div class="col">
                              {/* <h6>Restaurant Timings:</h6> */}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                </div>
              </div>
              <div className="col-lg-3 col-md-3 col-sm-12">
                <div className="container bg-white py-3 order-card">
                  <h6 className="border-bottom pb-2 mb-3">
                    <FontAwesomeIcon icon="shopping-basket" className="mr-2" />
                    Your Order
                  </h6>
                  {cartItemsList.length > 0 ? (
                    <div>
                      <div>
                        <ul>{this._renderCartItemsList()}</ul>
                      </div>
                      <div>
                        <div className="row ">
                          <div className="col-12">
                            <p
                              style={{
                                backgroundColor: "#f1f3f8",
                                padding: "10px 15px",
                              }}
                            >
                              Total+{" "}
                              <span
                                style={{
                                  float: "right",
                                  color: "#2f313a",
                                  fontSize: "14px",
                                  fontWeight: 700,
                                }}
                              >
                                <em>RS.{totalPrice}</em>
                              </span>
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  ) : (
                    <p className="text-success">
                      There are no items in your basket.
                    </p>
                  )}
                  <div>
                    <div className="row ">
                      <div className="col-12">
                        <button
                          onClick={() => this.handleConfirmOrderBtn()}
                          type="button"
                          className="btn btn-warning btn-sm btn-block text-uppercase mr-2 mr-1 px-3"
                        >
                          <b>Confirm Order</b>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  //console.log("mapstate", state);
  return {
    user: state.user,
  };
};

export default connect(mapStateToProps, null)(RestaurantDetails);
