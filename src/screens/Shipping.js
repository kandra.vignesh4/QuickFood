import React, { Component } from "react";
import Navbar2 from "../components/Navbar2";
import Footer from "../components/Footer";
import axios from "axios";
import { connect } from "react-redux";
import { my_order } from "../ServiceLayer/Order";
import "bootstrap/dist/css/bootstrap.css";
import "../App.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import RestaurantDetails from "./RestaurantDetails";

class Shipping extends Component {
  constructor() {
    super();
    this.state = {
      tab1: "col-12 col-lg-4 col-md-4 text-center order-req-tab-active",
      tab2: "col-12 col-lg-4 col-md-4 text-center",
      tab2Content: false,
      tab1Content: true,
      currentDateTime: new Date().toLocaleString(),
    };
  }

  async componentDidMount() {
    this.props.my_order();
  }

  static getDerivedStateFromProps(props) {
    const { user } = props;
    return {
      userDetails: user,
    };
  }
  handleTabs(e) {
    if (e === "tab1") {
      this.setState({
        tab1: "col-12 col-lg-4 col-md-4 text-center order-req-tab-active",
        tab2: "col-12 col-lg-4 col-md-4 text-center",
        tab1Content: true,
        tab2Content: false,
      });
    } else if (e === "tab2") {
      this.setState({
        tab1: "col-12 col-lg-4 col-md-4 text-center",
        tab2: "col-12 col-lg-4 col-md-4 text-center order-req-tab-active",
        tab1Content: false,
        tab2Content: true,
      });
    }
  }

  _renderPendingMyOrder() {
    const { myOrder } = this.props;
    // console.log(myOrder)
    if (myOrder) {
      return Object.keys(myOrder).map((val) => {
        const userUid = myOrder[val].userUid;
        const orderId = myOrder[val].id;
        if (myOrder[val].status === "PENDING") {
          return (
            <div
              className="container border-bottom pb-2 px-lg-0 px-md-0 mb-4"
              key={myOrder[val].id}
            >
              <div className="row mb-3">
                <div className="col-lg-6 col-md-6 col-12">
                  <h5 className="">{myOrder[val].restaurant.userName}</h5>
                </div>
                <div className="col-lg-6 col-md-6 col-12 text-lg-right text-md-right text-center ">
                  <span className="text-uppercase text-danger order-req-status">
                    Price
                  </span>
                </div>
              </div>
              {Object.keys(myOrder[val].itemList).map((val2) => {
                // console.log(myOrder[val].itemList[val2])
                // console.log(val2)
                return (
                  <div className="row mb-3" key={val2}>
                    <div className="col-lg-2 col-md-3 col-8 offset-2 offset-lg-0 offset-md-0 px-0 mb-3 text-center">
                      <img
                        style={{ width: "70px", height: "70px" }}
                        alt="Natural Healthy Food"
                        src={myOrder[val].itemList[val2].itemImageUrl}
                      />
                    </div>
                    <div className="col-lg-7 col-md-6 col-sm-12 px-0">
                      <h6 className="">
                        {myOrder[val].itemList[val2].itemTitle}
                      </h6>
                      <p className="mb-1">
                        <small>
                          {myOrder[val].itemList[val2].itemIngredients}
                        </small>
                      </p>
                    </div>
                    <div className="col-lg-3 col-md-3 col-sm-12 px-0 text-right">
                      <span style={{ fontSize: "14px" }} className="mx-3">
                        <b>RS.{myOrder[val].itemList[val2].itemPrice}</b>
                      </span>
                    </div>
                  </div>
                );
              })}
              <div className="row mb-3 mb-md-0 mb-lg-0">
                <div className="col-lg-6 col-md-6 col-12 order-lg-first order-md-first order-last "></div>
                <div className="col-lg-6 col-md-6 col-12 text-lg-right text-md-right">
                  <p>{this.state.currentDateTime}</p>
                  <p>
                    <b className="mr-4">Total Price:</b>
                    <span style={{ fontSize: "1.1rem" }}>
                      RS.{myOrder[val].totalPrice}
                    </span>
                  </p>
                </div>
              </div>
            </div>
          );
        }
      });
    }
  }
  handleConfirmDetailsBtn() {
    const { userDetails } = this.state;
    if (userDetails) {
      if (!userDetails.isRestaurant) {
        this.props.history.push("/payment");
      }
    }
  }
  _renderUserDetails() {
    const { userDetails } = this.state;
    return (
      <div className="user-details">
        <div className="row">
          <div className="col">
            <h6>Full Name : </h6>
            {userDetails.userName}
            <h6>Phone Number : </h6>
            {userDetails.phoneNumber}
            <h6>Email : </h6>
            {userDetails.userEmail}
            <h6>Address : </h6>
            {userDetails.userAddress}, {userDetails.userCity},{" "}
            {userDetails.userCountry},{""}
            {userDetails.userPincode}
          </div>
          <div className="col">
            <img
              style={{ width: "15em", height: "15em" }}
              alt="Natural Healthy Food"
              src={userDetails.userProfileImageUrl}
            />
          </div>
        </div>

        <div>
          <div className="row ">
            <div className="col-12">
              <button
                onClick={() => this.handleConfirmDetailsBtn()}
                type="button"
                className="btn btn-warning btn-sm btn-block text-uppercase mr-2 mr-1 px-3"
                style={{ width: "200px" }}
              >
                <b>Confirm details</b>
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
  render() {
    const { tab1, tab1Content, tab2, tab2Content, userDetails } = this.state;
    return (
      <div>
        <div className="container-fluid res-details-cont1">
          <div className="">
            {/* <Navbar history={this.props.history} /> */}
            <Navbar2 history={this.props.history} />
            <div className="container px-0 res-details-cont1-text mx-0">
              <div className="container">
                {userDetails ? (
                  <div className="row">
                    <div className="col-lg-2 col-md-3 col-6 text-lg-center text-md-center pr-0 mb-2">
                      <img
                        className="p-2 bg-white rounded text-center"
                        alt="Natural Healthy Food"
                        style={{ width: "60%" }}
                        src={userDetails.userProfileImageUrl}
                      />
                    </div>
                    <div className="col-lg-10 col-md-9 col-12 pl-lg-0 pl-md-0">
                      <h1 className="restaurant-title">
                        {userDetails.userName}
                      </h1>
                      {/* <p className="restaurant-text">{userDetails.typeOfFood.join(', ')}</p> */}
                    </div>
                  </div>
                ) : null}
              </div>
            </div>
          </div>
        </div>
        <div style={{ background: "#EBEDF3" }} className="container-fluid py-5">
          <div className="container">
            <div className="row">
              <div className="col-lg-10 col-md-10 col-sm-12 offset-lg-1 offset-md-1">
                <div className="container">
                  <div className="row">
                    <div
                      className={tab1}
                      onClick={() => this.handleTabs("tab1")}
                    >
                      <p className="order-req-tab-text">Shipping Details</p>
                    </div>
                    <div
                      className={tab2}
                      onClick={() => this.handleTabs("tab2")}
                    >
                      <p className="order-req-tab-text">User Details</p>
                    </div>
                  </div>
                  {tab1Content && (
                    <div className="row pending-order-section">
                      <div className="col-12 bg-white p-4">
                        {this._renderPendingMyOrder()}
                      </div>
                    </div>
                  )}
                  {tab2Content && (
                    <div className="row inProgress-order-section">
                      <div className="col-12 bg-white p-4">
                        {this._renderUserDetails()}
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>

        <Footer />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  // console.log("mapStateToProps states =>> ", state);
  return {
    user: state.user,
    myOrder: state.myOrder,
    // todos: state.todos
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    my_order: () => dispatch(my_order()),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Shipping);
