import axios from "axios";

const restaurant_list = () => {
  return (dispatch) => {
    var restaurantList = [];
    const options = {
      url: process.env.REACT_APP_NodeJS_ServiceURL + "/api/v1/restaurants",
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json;charset=UTF-8",
      },
    };
    axios(options)
      .then((response) => {
        console.log("respone.data.data", response.data.data);
        restaurantList = response.data.data;
        restaurantList.forEach((doc) => {
          doc.id = doc._id;
        });
        dispatch({
          type: "RESTAURANT_LIST",
          restaurantList: restaurantList,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};

const foodItem_list = () => {
  return (dispatch) => {
    var foodItemList = [];
    const options = {
      url: process.env.REACT_APP_NodeJS_ServiceURL + "/api/v1/orders",
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json;charset=UTF-8",
      },
    };
    axios(options)
      .then((response) => {
        console.log("respone.data.data", response.data.data);
        foodItemList = response.data.data;
        foodItemList.forEach((doc) => {
          doc.id = doc._id;
        });
        dispatch({
          type: "FOODITEM_LIST",
          foodItemList: foodItemList,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};

const my_foods = () => {
  return (dispatch) => {
    var myFoods = [];
    var token = localStorage.getItem("securityToken");
    const options = {
      url: process.env.REACT_APP_NodeJS_ServiceURL + "/api/v1/menuitems",
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json;charset=UTF-8",
        Authorization: `Bearer ${token}`,
      },
    };
    axios(options)
      .then((response) => {
        //console.log(response.data.data);
        myFoods = response.data.data;
        myFoods.forEach((doc) => {
          doc.id = doc._id;
        });
        dispatch({
          type: "MY_FOODS",
          myFoods: myFoods,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
};

function addItem(itemDetails) {
  const {
    itemTitle,
    itemIngredients,
    itemPrice,
    itemImage,
    chooseItemType,
  } = itemDetails;
  var token = localStorage.getItem("securityToken");
  return new Promise((resolve, reject) => {
    const url = process.env.REACT_APP_NodeJS_ServiceURL + "/api/v1/images/user";
    const formData = new FormData();
    formData.append("image", itemImage);
    const options = {
      headers: {
        Accept: "multipart/form-data",
        "Content-Type": "multipart/form-data",
        Authorization: `Bearer ${token}`,
      },
    };
    axios
      .post(url, formData, options)
      .then((response) => {
        //console.log(response.data.data.url);
        var ImageURL = response.data.data.url;
        const itemDetailsForDb = {
          itemTitle: itemTitle,
          itemIngredients: itemIngredients,
          itemPrice: itemPrice,
          itemImageUrl: ImageURL,
          chooseItemType: chooseItemType,
        };
        const AddItem = {
          url: process.env.REACT_APP_NodeJS_ServiceURL + "/api/v1/menuitems",
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json;charset=UTF-8",
            Authorization: `Bearer ${token}`,
          },
          data: itemDetailsForDb,
        };
        axios(AddItem)
          .then((result) => {
            console.log("Item Added: ", result.status);
            resolve("Successfully added food item");
          })
          .catch((error) => {
            console.log(error);
            var errorMessage = error.message;
            reject(errorMessage);
          });
      })
      .catch((error) => {
        console.log("Error adding document: ", error);
        var errorMessage = error.message;
        reject(errorMessage);
      });
  });
}

function editItem(itemDetails) {
  const {
    itemTitle,
    itemIngredients,
    itemPrice,
    itemImage,
    chooseItemType,
    menuItemId,
  } = itemDetails;
  var token = localStorage.getItem("securityToken");
  return new Promise((resolve, reject) => {
    //     const url = process.env.REACT_APP_NodeJS_ServiceURL + '/api/v1/images/user';
    //     const formData = new FormData();
    //     formData.append('image', itemImage);
    //     const options = {
    //         headers: {
    //             'Accept': 'multipart/form-data',
    //             'Content-Type': 'multipart/form-data',
    //             "Authorization": `Bearer ${token}`
    //         }
    //     };
    //     axios.post(url, formData, options)
    //         .then((response) => {
    //             //console.log(response.data.data.url);
    //             var ImageURL = response.data.data.url;
    const itemDetailsForDb = {
      itemTitle: itemTitle,
      itemIngredients: itemIngredients,
      itemPrice: itemPrice,
      itemImageUrl: itemImage,
      chooseItemType: chooseItemType,
      menuItemId: menuItemId,
    };
    const Item = {
      url: process.env.REACT_APP_NodeJS_ServiceURL + "/api/v1/menuitems",
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json;charset=UTF-8",
        Authorization: `Bearer ${token}`,
      },
      data: itemDetailsForDb,
    };
    axios(Item)
      .then((result) => {
        console.log("Item updated: ", result);
        resolve("Successfully updated food item");
      })
      .catch((error) => {
        console.log(error);
        var errorMessage = error.message;
        reject(errorMessage);
      });
    // }).catch((error) => {
    //     console.log("Error adding document: ", error);
    //     var errorMessage = error.message;
    //     reject(errorMessage);
    // });
  });
}

const removeItem = (menuitemID) => {
  return new Promise((resolve, reject) => {});
};

const deleteMenuItem = (menuitemID) => {
  var token = localStorage.getItem("securityToken");
  return new Promise((resolve, reject) => {
    const Item = {
      url:
        process.env.REACT_APP_NodeJS_ServiceURL +
        `/api/v1/menuitems/${menuitemID}`,
      method: "DELETE",
      mode: "no-cors",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json;charset=UTF-8",
        Authorization: `Bearer ${token}`,
      },
    };
    axios(Item)
      .then((result) => {
        console.log(result);
        resolve("Successfully deleted food item");
      })
      .catch((error) => {
        console.log(error);
        var errorMessage = error.message;
        reject(errorMessage);
      });
  });
};

const ReviewDetails = (
  rating,
  review,
  resDetails,
  userDetails,
  history
) => {
  var token = localStorage.getItem("securityToken");
  console.log('token           ',token);
  console.log('restaurant Id  ',resDetails.id);

  return new Promise((resolve, reject) => {
    // let user = firebase.auth().currentUser;
    // var uid;
    // if (user != null) {
    //     uid = user.uid;
    // };

    const options = {
      url:
        process.env.REACT_APP_NodeJS_ServiceURL +
        "/api/v1/restaurants/" +
        resDetails.id,
        
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json;charset=UTF-8",
        Authorization: `Bearer ${token}`,
      },
      data: {
        userReview: rating,
        userRating: review,
      },
    };
    axios(options)
      .then((response) => {
        //console.log(response.status);
        resolve("Successfully Review Given");
      })
      .catch((error) => {
        console.log(error);
        reject(error.message);
      });
  });
};

export {
  restaurant_list,
  foodItem_list,
  my_foods,
  addItem,
  removeItem,
  editItem,
  deleteMenuItem,
  ReviewDetails,

};
